use std::{collections::HashMap, io::{self, Write}};


#[derive(Eq,PartialEq, PartialOrd, Ord,Clone, Copy,Hash)]
struct PointCoord {
    x:usize,
    y:usize
}


impl From<(usize,usize)> for PointCoord {
    fn from(value: (usize,usize)) -> Self {
        Self { x: value.0, y: value.1 }
    }
}
impl Into<(usize,usize)> for PointCoord {
    fn into(self) -> (usize,usize) {
        (self.x,self.y)
    }
}

impl PointCoord {
    fn new(x:usize,y:usize)->Self{
        Self { x, y  }
    }
    fn get_neighbors(&self,map:&Map,rev:bool)->Vec<PointCoord>{
        let max_size = map.size;
        let a = map.get_point(self);
        vec![
            //north
            {
                if self.y >= (max_size.y-1) { None }
                else {  //this is stupid and verbos, but I don't care and I"m not going to fix it.
                    let b = Self::new(self.x,self.y + 1);
                    let ch = Self::check_move(a,map.get_point(&b),rev);
                    if ch {
                        b.into()
                    } else {
                        None
                    }
                 }
            },
            //south
            {
                if self.y <= 0 { None }
                else { 
                    
                    let b = Self::new(self.x,self.y - 1);
                    let ch = Self::check_move(a,map.get_point(&b),rev);
                    if ch {
                        b.into()
                    } else {
                        None
                    }
                 }
            },
            //east
            {
                if self.x >= (max_size.x-1) { None }
                else { 
                    
                    let b = Self::new(self.x + 1,self.y);
                    let ch = Self::check_move(a,map.get_point(&b),rev);
                    if ch {
                        b.into()
                    } else {
                        None
                    }
                 }
            },
            //west
            {
                if self.x <= 0 { None }
                else { 
                    
                    let b = Self::new(self.x - 1,self.y);
                    let ch = Self::check_move(a,map.get_point(&b),rev);
                    if ch {
                        b.into()
                    } else {
                        None
                    }
                 }
            },
        ].into_iter().filter_map(|f|f).collect()
    }
    fn check_move(a:&PointType,b:&PointType,rev:bool)->bool {
        match (a,b) {
            (PointType::Start, PointType::Start) => panic!(),
            (PointType::Start, PointType::End) => false,
            (PointType::Start, PointType::Point(p)) => *p == 0,
            (PointType::End, PointType::Start) => false,
            (PointType::End, PointType::End) => panic!(),
            (PointType::End, PointType::Point(p)) => *p == 25,
            (PointType::Point(p), PointType::Start) => *p == 0,
            (PointType::Point(p), PointType::End) => *p == 25,
            (PointType::Point(a), PointType::Point(b)) => {
                if rev {
                    (*b + 1) >= *a
                }
                else {
                    (*a + 1) >= *b
                }
                // a.abs_diff(*b) <= 1
                // ((*b) as i32 - (*a) as i32).abs() <= 1
            },
        }

    }
}

#[derive(PartialEq, Eq)]
enum PointType {
    Start,
    End,
    Point(u16),
}

const A_VALUE:u16 = "a".as_bytes()[0] as u16;

impl From<char> for PointType {
    fn from(i: char) -> Self {
        match i {
            'S' =>{
                PointType::Start
            }
            'E' =>{
                PointType::End
            }
            _ => {
                let a = i.to_string().as_str().as_bytes()[0] as u16 - A_VALUE;
                PointType::Point(a)
            }
        }
    }
}

struct Map {
    map:Vec<Vec<PointType>>,
    // start:PointCoord,
    size:PointCoord
}

impl From<&str> for Map {
    fn from(source: &str) -> Self {
        let map:Vec<Vec<PointType>> = source.split("\n").into_iter().map(|f|{
            let a:Vec<PointType> = f.chars().into_iter().map(|f|{
                f.into()
            }).collect();
            a
        }).collect();
        let map_size = (map.len(),map[0].len()).into();
        Self {
            map,
            size: map_size
        }
    }
}

impl Map {
    fn scan_route(&self,start_coords:&Vec<PointCoord>)->Option<Vec<PointCoord>>{
        let mut reached:HashMap<PointCoord, i32> = HashMap::new();
        // let start_coord = self.find(PointType::Start{});
        let end_coord = self.find(PointType::End{})[0];
        let mut dist:i32 = 0;
        for coord in start_coords {
            reached.insert(*coord, dist);
        }

        let mut old_front = start_coords.clone();
        let mut new_front = old_front.clone();

        while new_front.len() != 0 {
            dist += 1;
            old_front = new_front.clone();
            new_front = vec![];

            for p in &old_front{
                let neighbors = p.get_neighbors(self,false);
                for n in neighbors {
                    if reached.contains_key(&n) {
                        //already reached
                    }else{
                        reached.insert(n, dist);
                        new_front.push(n);
                    }
                }
            }
            // self.print(&reached);
        } 

        //trace back from end point
        
        let mut trace = Vec::with_capacity(dist as usize);//;vec![end_coord];
        trace.push(end_coord);

        assert!(reached.get(&end_coord).is_some(),"end is not reachable!");

        let mut tail_dist = *reached.get(&end_coord).unwrap();
        
        while *self.get_point(&trace.last().unwrap()) != PointType::Start {
            let ln = trace.len();
            let a = trace.last().unwrap();
            for i in a.get_neighbors(self,true){
                let step_dist = *reached.get(&i).unwrap();
                if tail_dist == (step_dist+1) {
                    trace.push(i);
                    tail_dist -= 1;
                    break;
                }
            }
            for i in start_coords {
                if trace.last().unwrap() == i {
                    trace.reverse();
                    return Some(trace);
                }
            }
            if trace.len() - ln != 1 {
                panic!("the path didn't increase by one!?")
            }
        }
        None
    }
    fn find(&self,a:PointType)->Vec<PointCoord>{
        let mut output = vec![];
        for x in 0..self.map.len() {
            for y in 0..self.map[x].len(){
                if self.map[x][y] == a {
                    output.push(PointCoord::new(x,y));
                }
            }
        }
        output
    }

    fn get_point(&self, a: &PointCoord) -> &PointType {
        &self.map[a.x][a.y]
    }
    fn _print(&self,reached:&HashMap<PointCoord,i32>) {
        for x in 0..self.size.x {
            for y in 0..(self.size.y) {
                match reached.get(&PointCoord { x, y }) {
                    Some(v) => print!("{: >3x}",v%0x1000),
                    None => {
                        print!(" XX")
                    }
                    ,
                };
            }
            print!("\n")
        }
        println!("===========================================");
        io::stdout().flush().unwrap();
    }
}

pub fn main() {
    let p:Map = REAL_PUZZLE_INPUT.into();
    {
        let start = p.find(PointType::Start);
        let route = p.scan_route(&start).unwrap();
        println!("Day12 : route len {}",route.len() -1 );
    }
    {
        let start = p.find(PointType::Point(0));
        let route = p.scan_route(&start).unwrap();
        println!("Day12b: route len {}",route.len() -1 );
    }
}


const _FAKE_PUZZLE_INPUT:&str = 
"Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi";

const _TOY_PUZZLE_INPUT:&str = 
"SabcdefghijklmnopqrstuvwxyzE";


const REAL_PUZZLE_INPUT:&str = 
"abcccccccaaaaaaaaccccccccccaaaaaaccccccaccaaaaaaaccccccaacccccccccaaaaaaaaaaccccccccccccccccccccccccccccccccaaaaa
abcccccccaaaaaaaaacccccccccaaaaaacccccaaacaaaaaaaaaaaccaacccccccccccaaaaaaccccccccccccccccccccccccccccccccccaaaaa
abcccccccaaaaaaaaaaccccccccaaaaaacaaacaaaaaaaaaaaaaaaaaaccccccccccccaaaaaaccccccccccccccaaacccccccccccccccccaaaaa
abaaacccccccaaaaaaacccccccccaaacccaaaaaaaaaaaaaaaaaaaaaaaaacccccccccaaaaaaccccccccccccccaaacccccccccccccccccaaaaa
abaaaaccccccaaaccccccccccccccccccccaaaaaaaaacaaaacacaaaaaacccccccccaaaaaaaacccccccccccccaaaaccaaacccccccccccaccaa
abaaaaccccccaaccccaaccccccccccccccccaaaaaaacaaaaccccaaaaaccccccccccccccccacccccccccccccccaaaaaaaaacccccccccccccca
abaaaaccccccccccccaaaacccccccccaacaaaaaaaacccaaacccaaacaacccccccccccccccccccccccccccciiiiaaaaaaaacccccccccccccccc
abaaacccccccccccaaaaaacccccccccaaaaaaaaaaacccaaacccccccaacccccccccccaacccccccccccccciiiiiiijaaaaccccccccaaccccccc
abaaaccccccccccccaaaacccccccccaaaaaaaacaaacccaaaccccccccccccccccccccaaacaaacccccccciiiiiiiijjjacccccccccaaacccccc
abcccccaacaacccccaaaaaccccccccaaaaaacccccacaacccccccccccccccccccccccaaaaaaaccccccciiiinnnoijjjjjjjjkkkaaaaaaacccc
abcccccaaaaacccccaacaaccccccccccaaaacccaaaaaaccccccccccccccccccccccccaaaaaaccccccciiinnnnooojjjjjjjkkkkaaaaaacccc
abccccaaaaacccccccccccccccccccccaccccccaaaaaaaccccccccccccccccccccaaaaaaaaccccccchhinnnnnoooojjooopkkkkkaaaaccccc
abccccaaaaaaccccccccccccccccccccccccccccaaaaaaacccccccccccccccccccaaaaaaaaacccccchhhnnntttuooooooopppkkkaaaaccccc
abccccccaaaaccccccccccacccccccccccccccccaaaaaaacccaaccccccccccccccaaaaaaaaaaccccchhhnnttttuuoooooppppkkkaaaaccccc
abccccccaccccccccccccaaaacaaaccccccccccaaaaaacaaccaacccaaccccccccccccaaacaaacccchhhnnnttttuuuuuuuuupppkkccaaccccc
abccccccccccccccaaccccaaaaaaaccccccccccaaaaaacaaaaaacccaaaaaaccccccccaaacccccccchhhnnntttxxxuuuuuuupppkkccccccccc
abcccccccccccccaaaacccaaaaaaacccaccccccccccaaccaaaaaaacaaaaaaccccccccaacccaaccchhhhnnnttxxxxuuyyyuupppkkccccccccc
abcccccccccccccaaaaccaaaaaaaaacaaacccccccccccccaaaaaaaaaaaaaccccccccccccccaaachhhhmnnnttxxxxxxyyyuvppkkkccccccccc
abcccccccccccccaaaacaaaaaaaaaaaaaaccccccccccccaaaaaacaaaaaaaccccccccccccccaaaghhhmmmttttxxxxxyyyyvvpplllccccccccc
abccacccccccccccccccaaaaaaaaaaaaaaccccccccccccaaaaaacccaaaaaacccaacaacccaaaaagggmmmttttxxxxxyyyyvvppplllccccccccc
SbaaaccccccccccccccccccaaacaaaaaaaacccccccccccccccaacccaaccaacccaaaaacccaaaagggmmmsttxxxEzzzzyyvvvppplllccccccccc
abaaaccccccccccccccccccaaaaaaaaaaaaacaaccccccccccccccccaaccccccccaaaaaccccaagggmmmsssxxxxxyyyyyyvvvqqqlllcccccccc
abaaacccccccccccccccccccaaaaaaaaaaaaaaaaacccccccccccccccccccccccaaaaaaccccaagggmmmsssxxxwywyyyyyyvvvqqlllcccccccc
abaaaaacccccccccccccccccccaacaaaccaaaaaaacccccccccccccccccccccccaaaaccccccaagggmmmssswwwwwyyyyyyyvvvqqqllcccccccc
abaaaaaccccccccccccccccccccccaaaccccaaaacccccccccccccccccaaccaacccaaccccccccgggmmmmssssswwyywwvvvvvvqqqlllccccccc
abaaaaacccccccccccccaccacccccaaaccccaaaacccccccccccccccccaaaaaacccccccccccaaggggmllllsssswwywwwvvvvqqqqlllccccccc
abaaccccccccccccccccaaaaccccccccccccaccaccccccccccccccccccaaaaacccccccccccaaagggglllllssswwwwwrrqqqqqqmmllccccccc
abaaccccccccccccccccaaaaaccccccaaccaaccccccccccccccccccccaaaaaaccaacccccccaaaaggfffllllsswwwwrrrrqqqqqmmmcccccccc
abacaaaccccccccccccaaaaaaccccccaaaaaaccccccaacccccccccccaaaaaaaacaaacaaccccaaaaffffflllsrrwwwrrrmmmmmmmmmcccccccc
abaaaaaccccccccccccaaaaaaccccccaaaaaccccccaaaaccccccccccaaaaaaaacaaaaaaccccaaaaccfffflllrrrrrrkkmmmmmmmccccaccccc
abaaaacccccccccccccccaaccccccccaaaaaacccccaaaacccccccccccccaaccaaaaaaaccccccccccccffflllrrrrrkkkmmmmmccccccaccccc
abaaacccccccccccccccccccccccccaaaaaaaaccccaaaacccccccccccccaaccaaaaaaacccccccccccccfffllkrrrkkkkmddddcccccaaacccc
abaaacccccccccccccccccccccccccaaaaaaaacccccccccccccccccccccccccccaaaaaaccccccccccccfffllkkkkkkkdddddddcaaaaaacccc
abaaaacccccccccccccccccccccccccccaaccccccccccccccccccccccccccccccaacaaacccccccccccccfeekkkkkkkddddddcccaaaccccccc
abcaaacccccccccccaaaccccccccaacccaaccccaaaaaccccaaaccccccccccccccaaccccccccccccccccceeeeekkkkdddddccccccaaccccccc
abccccccccccccccaaaaaaccccccaaacaaccacaaaaaaaccaaaaccccccccccaccaaccccccccccccccccccceeeeeeeedddacccccccccccccccc
abccccccccccccccaaaaaacccccccaaaaacaaaaaccaaaaaaaacccccccccccaaaaacccccccccccccccccccceeeeeeedaaacccccccccccccaaa
abccccccaaacccccaaaaacccccccaaaaaacaaaaaaaaaaaaaaaccccccccccccaaaaaccccccccccccccccccccceeeeecaaacccccccccccccaaa
abccccccaaaccccccaaaaacccccaaaaaaaaccaaaaacaaaaaaccccccccccccaaaaaacccccccccccccccccccccaaaccccaccccccccccccccaaa
abccccaacaaaaacccaaaaacccccaaaaaaaacaaaaaaaaaaaaaaaccccaaaaccaaaacccccccccccccccccccccccaccccccccccccccccccaaaaaa
abccccaaaaaaaaccccccccccccccccaaccccaacaaaaaaaaaaaaaaccaaaaccccaaacccccccccccccccccccccccccccccccccccccccccaaaaaa";