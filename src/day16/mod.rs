use std::{borrow::BorrowMut, collections::HashMap, fmt::{Display, write}, hash::Hash, ops::Add, sync::Arc};

use rayon::prelude::*;

type Pressure = u128;

// type Address= [u8;2];
#[derive(PartialEq, Eq, Hash, Clone, Copy,Debug)]
struct Address {
    a: [u8; 2],
}

#[derive(PartialEq, Eq, Clone,Debug)]
struct Valve {
    address: Address,
    rate: Pressure,
    connects: Vec<Address>,
    distance_to: HashMap<Address, u32>,
    state: bool,
}
impl Valve {
    fn propage_routes(&mut self, map: &mut Map) {
        if self.rate == 0 {
            return; //early out for routes not worth exploring.
        }
        self.distance_to.insert(self.address.clone(), 0);
        let mut neighbors = self.connects.clone();
        let distance = 1;
        while neighbors.len() != 0 {
            let mut found = HashMap::new();
            neighbors.into_iter().for_each(|f| {
                let v = map.get_valve_mut(&f);
                if v.distance_to.contains_key(&self.address) {
                } else {
                    v.distance_to.insert(self.address.clone(), distance);
                    for next in &v.connects {
                        found.insert(next.clone(), true);
                    }
                }
            });
            neighbors = found.into_iter().map(|k| k.0.clone()).collect();
        }
    }
}

#[derive(PartialEq, Eq, Clone)]
struct Map {
    valves: HashMap<Address, Valve>,
    location: Address,
    released_pressure: Pressure,
    time_passed: u32,
}

#[derive(Debug)]
enum Action {
    Travel(Address),
    OpenValve,
    DoNothing,
}

impl Map {
    fn new(valves: HashMap<Address, Valve>) -> Self {
        let mut map = Self {
            valves,
            location: "AA".into(),
            released_pressure: 0,
            time_passed: 0,
        };
        map.fix_paths();
        // Self::fix_paths(&mut map);
        map
    }
    fn pass_minute(&mut self) {
        self.released_pressure = self.released_pressure
            + self
                .valves
                .values()
                .map(|f| if f.state { f.rate as u128 } else { 0 })
                .sum::<Pressure>();
        self.time_passed += 1;
    }
    fn travel(&mut self, target: Address) {
        self.location = target;
        self.pass_minute();
    }
    fn open_valve(&mut self) {
        self.valves.get_mut(&self.location).unwrap().state = true;
        self.pass_minute();
    }
    fn do_nothing(&mut self) {
        self.pass_minute();
    }
    fn get_avalible_moves(&self) -> &Vec<Address> {
        &self.valves.get(&self.location).unwrap().connects
    }
    fn get_avalible_actions(&self) -> Vec<Action> {
        if self.has_closed_useful_valve() {
            let mut a: Vec<Action> = self
                .get_avalible_moves()
                .iter()
                .map(|f| Action::Travel(f.clone()))
                .collect();
            let v = self.valves.get(&self.location).unwrap();
            if v.state == false && v.rate != 0 {
                a.push(Action::OpenValve)
            }
            a
        } else {
            vec![Action::DoNothing]
        }
    }

    fn get_closed_valves(&self) -> Vec<Address> {
        (&self.valves)
            .into_iter()
            .filter_map(|f| {
                // print!("{}",f.0);
                if f.1.state == false && f.1.rate != 0 {
                    Some(f.0.clone())
                } else {
                    None
                }
            })
            .collect()
    }
    fn has_closed_useful_valve(&self) -> bool {
        for i in self.valves.values() {
            if i.state == false && i.rate != 0 {
                return true;
            }
        }
        false
    }
    fn do_action(&mut self, a: &Action) {
        match a {
            Action::Travel(target) => self.travel(target.clone()),
            Action::OpenValve => self.open_valve(),
            Action::DoNothing => self.do_nothing(),
        }
    }
    fn brute_force_solve(&self, value: u32) -> Pressure {
        let do_func = |action| {
            let mut s = self.clone();
            s.do_action(&action);
            if s.time_passed >= value {
                s.released_pressure
            } else {
                s.brute_force_solve(value)
            }
        };
        let mut max = 0;

        let actions = self.get_avalible_actions();
        match actions.len() {
            0 => {
                panic!("this should happen. waiting should be an option!")
            }
            1 => {
                let a: Vec<Pressure> = actions.into_iter().map(do_func).collect();
                max = a[0];
            }
            _ => {
                let a: Vec<Pressure> = actions.into_par_iter().map(do_func).collect();
                a.into_iter().for_each(|f| {
                    max = max.max(f);
                });
            }
        }
        max
    }

    fn get_viable_actions(&self)->Vec<Action>{
        let closed_valves = self.get_closed_valves();
        if closed_valves.len() != 0 {
            let mut output = vec![];
            let current_location = self.valves.get(&self.location).unwrap();
            if current_location.state == false && current_location.rate != 0 {
                output.push(Action::OpenValve);
            }
            let mut move_map = HashMap::new();
            for possible_move in &current_location.connects {
                let possible_move_target = self.get_valve(possible_move);
                for closed_valve in &closed_valves {
                    if closed_valve != &self.location {
                        let distance_to = possible_move_target.distance_to.get(&closed_valve).unwrap();
                        match move_map.get(closed_valve) {
                            Some((_,dist)) => {
                                if dist > distance_to {
                                    move_map.insert(closed_valve.clone(),(possible_move.clone(),distance_to.clone()));
                                }
                            },
                            None => {
                                move_map.insert(closed_valve.clone(),(possible_move.clone(),distance_to.clone()));
                            },
                        }
                    }
                }
            }
            let mut action_map = HashMap::new();
            for i in move_map.values() {
                action_map.insert(i.0.clone(),true);
            }
            action_map.keys().for_each(|f|{
                output.push(Action::Travel(f.clone()));
            });
            
            // for i in 
            // let mut found_paths= HashMap::new();
            // for closed_valve in closed_vavles {
            //     for possible_move_target in current_location.connects {
            //         let connection = self.get_valve(&possible_move_target);
            //         match found_paths.get(&possible_move_target) {
            //             Some(i) => {},
            //             None => {

            //             },
            //         }

            //         // match 
            //         //     Some(value) => {
                            
            //         //     },
            //         //     None => {
            //         //         found_paths.insert(closed_valve, );
            //         //     },
            //         // }
            //     }
            // }
            // for i in &current_location.connects {
            //     for j in self.get_valve(i).distance_to {
                    
            //     }
            // }
            output

        } else {
            vec![Action::DoNothing]
        }
    }

    fn solve_pass_two(&mut self,value:u32) -> Pressure {
        if value <= self.time_passed {
            return self.released_pressure
        }
        let action_list = self.get_viable_actions();
        let a:Vec<Pressure> = match action_list.len() {
            0 => panic!(),
            1 => {
                self.do_action(&action_list[0]);
                vec![self.solve_pass_two(value)]
            }
            _ =>{
                action_list.par_iter().map(|action|{
                    let mut new_map = self.clone();
                    new_map.do_action(action);
                    new_map.solve_pass_two(value)
                }).collect()
            }
        };
        *a.iter().max().unwrap()
        // todo!()
    }

    fn get_valve_mut(&mut self, n: &Address) -> &mut Valve {
        self.valves.get_mut(n).unwrap()
    }
    fn get_valve(&self, n: &Address) -> &Valve {
        self.valves.get(n).unwrap()
    }

    fn fix_paths(&mut self) {
        let closed_valves = self.get_closed_valves();
        closed_valves.into_iter().for_each(|closed_valve| {
            let current_valve = self.get_valve_mut(&closed_valve);
            
            let mut dist = 0;
            current_valve.distance_to.insert(closed_valve, dist);

            let mut neighbors = current_valve.connects.clone();
            while neighbors.len() != 0 {
                dist += 1;
                let mut next: HashMap<Address, bool> = HashMap::new();

                for n in neighbors {
                    let q = self.get_valve_mut(&n);
                    if n != closed_valve {
                        match q.distance_to.get(&closed_valve){
                            Some(_) => {
                                println!("path already exists between {} and {}",closed_valve,n);
                            },
                            None => {
                                q.distance_to.insert(closed_valve, dist);
                                q.connects.iter().for_each(|next_neighbor| {
                                    next.insert(next_neighbor.clone(), true);
                                })
                            },
                        }
                    }
                }
                neighbors = next.keys().map(|f| *f).collect();
            }
        });
    }
}

pub fn main() {
    let mut map: Map = _FAKE_PUZZLE_INPUT.into();
    println!("{}",map);
    let a = map.solve_pass_two(30);
    // let a = map.brute_force_solve(30);
    println!("Day16 : TODO! {}", a);
}

impl From<&str> for Address {
    fn from(input: &str) -> Self {
        let i = input.as_bytes();
        Address { a: [i[0], i[1]] }
    }
}

impl From<&str> for Valve {
    fn from(input: &str) -> Self {
        let c: Vec<&str> = input.split(" ").collect();
        Valve {
            rate: c[4]
                .trim_end_matches(';')
                .trim_start_matches("rate=")
                .parse()
                .unwrap(),
            address: c[1].into(),
            connects: (9..c.len())
                .map(|index| c[index].trim_end_matches(",").into())
                .collect(),
            distance_to: HashMap::new(),
            state: false,
        }
    }
}

impl From<&str> for Map {
    fn from(input: &str) -> Self {
        let mut valves = HashMap::new();
        input.split("\n").for_each(|f| {
            let v: Valve = f.into();
            valves.insert(v.address.clone(), v);
        });
        Self::new(valves)
        // Self { valves, location: todo!() }
    }
}

impl Display for Address {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", String::from_utf8_lossy(&self.a))
    }
}

impl Display for Action {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Action::Travel(v) => { write!(f,"Travel({})",v)},
            Action::OpenValve => { write!(f,"OpenValve",)},
            Action::DoNothing => { write!(f,"DoNothing",)},
        }
    }
}

impl Display for Map {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f,"Location:{}",self.location)?;
        writeln!(f,"{}",self.location)?;
        for address_index in self.valves.keys() {
            let valve = self.valves.get(address_index).unwrap();
            writeln!(f,"{}:{}",address_index,valve)?;
        }
        writeln!(f,"")
    }
}
impl Display for Valve {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f,"Address:{},\tRate:{},\tState:{}\nConnects:",self.address,self.rate,self.state)?;

        for i in &self.connects {
            write!(f,"{},",i)?;
        }
        writeln!(f,"\nDistTo:")?;
        for g in &self.distance_to {
            write!(f,"{}:{},",g.0,g.1)?
        }
        write!(f,"\n==================")
        // write!(f,"{self:?}")}
        // write!(f,"{}",self.address)

        // address: Address,
        // rate: Pressure,
        // connects: Vec<Address>,
        // distance_to: HashMap<Address, u32>,
        // state: bool,
    
    }
}


const _FAKE_PUZZLE_INPUT: &str = include_str!("_FAKE_PUZZLE_INPUT.txt");
const _REAL_PUZZLE_INPUT: &str = include_str!("_REAL_PUZZLE_INPUT.txt");
