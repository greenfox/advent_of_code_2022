use std::fmt::Display;

enum Action {
    NOOP,
    ADDX(i32),
}

impl Action {
    fn get_durration(&self) -> usize {
        match self {
            Action::NOOP => 1,
            Action::ADDX(_) => 2,
        }
    }
}

impl Display for Action {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Action::NOOP => {
                write!(f, "noop")
            }
            Action::ADDX(a) => {
                write!(f, "addx {}", a)
            }
        }
    }
}

impl From<&str> for Action {
    fn from(value: &str) -> Self {
        let a: Vec<&str> = value.split(" ").into_iter().collect();
        match a[0] {
            "noop" => Action::NOOP,
            "addx" => Action::ADDX(a[1].parse().unwrap()),
            _ => {
                panic!("bad input!")
            }
        }
    }
}

struct ActionSet {
    actions: Vec<Action>,
    cycles_in_current_action: usize,
    cycles_total: i32,
    action_index: usize,
    register: i32,
}

impl Display for ActionSet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let a = match self.actions.get(self.action_index) {
            Some(a) => match a {
                Action::NOOP => "noop".to_string(),
                Action::ADDX(addx) => format!("addx {}", addx),
            },
            None => "done".to_string(),
        };
        write!(
            f,
            "cycles total: {} index: {} register: {} current_action: {}",
            self.cycles_total, self.action_index, self.register, a
        )
    }
}

impl From<&str> for ActionSet {
    fn from(value: &str) -> Self {
        Self {
            actions: value.split("\n").map(|i| i.into()).collect(),
            action_index: 0,
            cycles_total: 1,
            register: 1,
            cycles_in_current_action: 0,
        }
    }
}

impl Iterator for ActionSet {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        self.cycles_total += 1;
        self.cycles_in_current_action += 1;

        // let output = Some(self.register);

        match self.actions.get(self.action_index) {
            Some(action) => {
                if action.get_durration() == self.cycles_in_current_action {
                    match action {
                        Action::NOOP => {}
                        Action::ADDX(a) => self.register += a,
                    }
                    self.cycles_in_current_action = 0;
                    self.action_index += 1;
                }
                Some(self.register)
            }
            None => None,
        }
    }
}

fn sprite_range(p:i32,i:i32)->bool{
    (p-1) <= i && i <= (p+1)
}

pub fn main() {
    {
        let mut p = ActionSet::from(PUZZLE_INPUT);
        let mut total = 0;
        while p.next().is_some() {
            if ((p.cycles_total as i32) - 20) % 40 == 0 {
                let this_frame = p.register * p.cycles_total;
                total += this_frame;
            }
            // println!("{}", p);
        }
        println!("Day10 : total signal {}", total);
    }
    {
        let mut p = ActionSet::from(REAL_PUZZLE_INPUT);
        let mut h = 1;
        print!("Day10a: signal image\n#");
        while p.next().is_some() {
            if sprite_range(p.register, h) {
                print!("#")
            }else{
                print!(" ")
            }
            h = if h >= 39 { 
                print!("\n"); 0 
            } else { h + 1 };
            // if p.register
        }
    }
}

const PUZZLE_INPUT: &str = REAL_PUZZLE_INPUT;
// const PUZZLE_INPUT:&str = SMALL_PUZZLE_INPUT;
// const PUZZLE_INPUT:&str = REAL_PUZZLE_INPUT;

const _SMALL_PUZZLE_INPUT: &str = "noop
addx 3
addx -5";





const _FAKE_PUZZLE_INPUT: &str = "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";

const REAL_PUZZLE_INPUT: &str = "noop
noop
noop
addx 6
addx -1
noop
addx 5
noop
noop
addx -12
addx 19
addx -1
noop
addx 4
addx -11
addx 16
noop
noop
addx 5
addx 3
addx -2
addx 4
noop
noop
noop
addx -37
noop
addx 3
addx 2
addx 5
addx 2
addx 10
addx -9
noop
addx 1
addx 4
addx 2
noop
addx 3
addx 2
addx 5
addx 2
addx 3
addx -2
addx 2
addx 5
addx -40
addx 25
addx -22
addx 2
addx 5
addx 2
addx 3
addx -2
noop
addx 23
addx -18
addx 2
noop
noop
addx 7
noop
noop
addx 5
noop
noop
noop
addx 1
addx 2
addx 5
addx -40
addx 3
addx 8
addx -4
addx 1
addx 4
noop
noop
noop
addx -8
noop
addx 16
addx 2
addx 4
addx 1
noop
addx -17
addx 18
addx 2
addx 5
addx 2
addx 1
addx -11
addx -27
addx 17
addx -10
addx 3
addx -2
addx 2
addx 7
noop
addx -2
noop
addx 3
addx 2
noop
addx 3
addx 2
noop
addx 3
addx 2
addx 5
addx 2
addx -5
addx -2
addx -30
addx 14
addx -7
addx 22
addx -21
addx 2
addx 6
addx 2
addx -1
noop
addx 8
addx -3
noop
addx 5
addx 1
addx 4
noop
addx 3
addx -2
addx 2
addx -11
noop
noop
noop";
