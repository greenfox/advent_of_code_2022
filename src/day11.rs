use std::{ cell::RefCell, fmt::Display};


type BigBig = u64;

enum Operation {
    Add(BigBig),
    Mult(BigBig),
    Square,
}

impl Operation {
    fn do_op(&self, i: BigBig) -> BigBig {
        let i = match self {
            Operation::Add(a) => i + a,
            Operation::Mult(m) => i * m,
            Operation::Square => i * i
        };
        i
    }
}

impl From<&Vec<&str>> for Operation {
    fn from(e: &Vec<&str>) -> Self {
        match e[4] {
            "+" => Operation::Add(e[5].parse().unwrap()),
            "*" => match e[5] {
                "old" => Operation::Square,
                _ => Operation::Mult(e[5].parse().unwrap()),
            },
            _ => {
                panic!("invalid input to Operation line!")
            }
        }
        // todo!()
    }
}

struct Monkey {
    _index: u32,
    items: Vec<BigBig>,
    op: Operation,
    test: BigBig,
    true_target: usize,
    false_target: usize,
    total_inspections: usize,
}

impl From<&str> for Monkey {
    fn from(input: &str) -> Self {
        let lines: Vec<Vec<&str>> = input
            .split("\n")
            .map(|f| f.split_whitespace().into_iter().collect())
            .collect();

        let index: u32 = lines[0][1].strip_suffix(":").unwrap().parse().unwrap();

        let mut items = vec![];
        for i in 2..lines[1].len() {
            let l = match lines[1][i].strip_suffix(",") {
                Some(s) => s.parse().unwrap(),
                None => lines[1][i].parse().unwrap(),
            };
            items.push(l);
        }

        let op = (&lines[2]).into();

        // 3 test 3
        let test = lines[3][3].parse().unwrap();

        // 4 if true
        let true_target = lines[4][5].parse().unwrap();

        // 5 if false
        let false_target = lines[5][5].parse().unwrap();

        let total_inspections = 0;
        Self {
            _index: index,
            items,
            op,
            test,
            true_target,
            false_target,
            total_inspections,
        }
    }
}

struct Monkeys {
    m: Vec<RefCell<Monkey>>,
}

impl Monkeys {
    fn iterate(&mut self,i:BigBig,lcm:BigBig) {
        for monkey in &self.m {
            let mut monkey = monkey.borrow_mut();
            for f in &monkey.items {
                let new_value = monkey.op.do_op(*f);
                let new_value = new_value / i;
                let new_value = new_value % lcm;
                let target = if new_value % monkey.test == 0 {
                    self.m.get(monkey.true_target).unwrap()
                } else {
                    self.m.get(monkey.false_target).unwrap()
                };
                target.borrow_mut().items.push(new_value);
            }
            monkey.total_inspections += monkey.items.len();
            monkey.items = vec![];
        }
    }
    fn get_monkey_buisiness_level(self) -> usize {
        let mut a: Vec<usize> = self
            .m
            .into_iter()
            .map(|f| f.borrow().total_inspections)
            .collect();
        a.sort_by(|a, b| {
            if a == b {
                std::cmp::Ordering::Equal
            } else if a < b {
                std::cmp::Ordering::Greater
            } else {
                std::cmp::Ordering::Less
            }
        });
        a[0] * a[1]
    }
}

impl From<&str> for Monkeys {
    fn from(input: &str) -> Self {
        // todo!()
        Self {
            m: input
                .split("\n\n")
                .map(|f| {
                    let a: Monkey = f.into();
                    RefCell::from(a)
                })
                .collect(),
        }
    }
}

impl Display for Monkeys {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut i = 0;
        let mut output = "".to_string();
        for m in &self.m {
            output = format!("{}Monkey {}:", output, i);
            let m = m.borrow();
            for i in &m.items {
                output = format!("{}{},", output, i);
            }
            output = format!("{}\n", output);
            i += 1;
        }
        write!(f, "{}", output)
    }
}

pub fn main() {
    {
        let mut ms: Monkeys = REAL_PUZZLE_INPUT.into();
        for _i in 0..20 {
            ms.iterate(3,BigBig::MAX);
            // println!("Itteration:{}\n{}", i + 1, ms);
            // print!("")
        }
        println!(
            "\nDay11 : monkey buisness level: {}",
            ms.get_monkey_buisiness_level()
        );
    }
    {
        let mut ms: Monkeys = REAL_PUZZLE_INPUT.into();
        let lcm = {
            let mut lcm = 1;
            for i in &ms.m {
                lcm = lcm * i.borrow().test;
            }
            lcm
        };
        for _i in 0..10000 {
            ms.iterate(1,lcm);

            // println!("Itteration:{}\n{}", i + 1, ms);
            // print!("")
        }
        println!(
            "Day11a: monkey buisness level: {}",
            ms.get_monkey_buisiness_level()
        );
    }
}

/*
8100000000 is too low
25935263541
 */

const _FAKE_PUZZLE_INPUT: &str = "Monkey 0:
Starting items: 79, 98
Operation: new = old * 19
Test: divisible by 23
  If true: throw to monkey 2
  If false: throw to monkey 3

Monkey 1:
Starting items: 54, 65, 75, 74
Operation: new = old + 6
Test: divisible by 19
  If true: throw to monkey 2
  If false: throw to monkey 0

Monkey 2:
Starting items: 79, 60, 97
Operation: new = old * old
Test: divisible by 13
  If true: throw to monkey 1
  If false: throw to monkey 3

Monkey 3:
Starting items: 74
Operation: new = old + 3
Test: divisible by 17
  If true: throw to monkey 0
  If false: throw to monkey 1";

const REAL_PUZZLE_INPUT: &str = "Monkey 0:
Starting items: 72, 64, 51, 57, 93, 97, 68
Operation: new = old * 19
Test: divisible by 17
  If true: throw to monkey 4
  If false: throw to monkey 7

Monkey 1:
Starting items: 62
Operation: new = old * 11
Test: divisible by 3
  If true: throw to monkey 3
  If false: throw to monkey 2

Monkey 2:
Starting items: 57, 94, 69, 79, 72
Operation: new = old + 6
Test: divisible by 19
  If true: throw to monkey 0
  If false: throw to monkey 4

Monkey 3:
Starting items: 80, 64, 92, 93, 64, 56
Operation: new = old + 5
Test: divisible by 7
  If true: throw to monkey 2
  If false: throw to monkey 0

Monkey 4:
Starting items: 70, 88, 95, 99, 78, 72, 65, 94
Operation: new = old + 7
Test: divisible by 2
  If true: throw to monkey 7
  If false: throw to monkey 5

Monkey 5:
Starting items: 57, 95, 81, 61
Operation: new = old * old
Test: divisible by 5
  If true: throw to monkey 1
  If false: throw to monkey 6

Monkey 6:
Starting items: 79, 99
Operation: new = old + 2
Test: divisible by 11
  If true: throw to monkey 3
  If false: throw to monkey 1

Monkey 7:
Starting items: 68, 98, 62
Operation: new = old + 3
Test: divisible by 13
  If true: throw to monkey 5
  If false: throw to monkey 6";
