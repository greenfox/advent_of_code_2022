use std::{
    collections::HashMap,
    fmt::Display,
    io::Write,
    ops::{Add, Sub},
    process::Output,
};

use rayon::prelude::*;

#[derive(Clone, Copy, Hash, PartialEq, Eq, Debug)]
struct Point {
    x: i32,
    y: i32,
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "(x:{},y:{})", self.x, self.y)
    }
}

impl Point {
    fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }
    fn dist(&self, p: &Point) -> i32 {
        (self.x - p.x).abs() + (self.y - p.y).abs()
    }
}
// const MIN_POINT:Point = Point::new(i32::MIN,i32::MIN);
// const MAX_POINT:Point = Point::new(i32::MAX,i32::MAX);

#[derive(Clone, Copy)]
struct Sensor {
    point: Point,
    bacon: Point,
    size: i32,
}
impl Sensor {
    fn in_range(&self, p: &Point) -> bool {
        self.point.dist(p) <= self.size
    }
    fn check_coverage(&self, y: i32) -> Option<Range> {
        //widest it `self.point.x - size` to `self.point.x + size`, inclusive
        let a = if self.point.y < y {
            //y is above
            let dist = y - self.point.y;
            if dist > self.size {
                None
            } else {
                let w = self.size - dist;
                Range::new(self.point.x - w, self.point.x + w).into()
            }
        } else {
            //y is below
            let dist = self.point.y - y;
            if dist > self.size {
                None
            } else {
                let w = self.size - dist;
                Range::new(self.point.x - w, self.point.x + w).into()
            }
        };
        match a {
            Some(s) => assert!(s.left <= s.right),
            None => {},
        };
        a
        // if self.point.y + self.size < y || self.point.y - self.size > y {
        //     None
        // } else {

        //     todo!()
        // }

        // todo!()
    }
}

#[derive(Clone, Copy)]
struct Range {
    left: i32,
    right: i32,
}
impl Range {
    fn new(left: i32, right: i32) -> Self {
        Self { left, right }
    }
}

struct Coverage {
    ranges: Vec<Range>,
}

impl Coverage {
    fn remove(&mut self, sub: Range) {
        self.ranges = self
            .ranges
            .iter()
            .map(|p| {
                let output = match sub.left > p.right || sub.right < p.left {
                    true => {
                        /*
                        sub      |----|
                        p  |----|

                        sub |----|
                        p           |------|
                         */
                        vec![*p]
                    }
                    false => {
                        match (sub.left <= p.left, p.right <= sub.right) {
                            (true, true) => {
                                /*
                                sub |-------------|
                                p       |------|
                                 */
                                vec![]
                            }
                            (true, false) => {
                                /*
                                sub |------|
                                p       |------|
                                 */
                                vec![Range {
                                    left: sub.right + 1,
                                    right: p.right,
                                }]
                                // todo!()
                            }
                            (false, true) => {
                                /*
                                sub      |------|
                                p    |------|
                                 */
                                vec![Range {
                                    left: p.left,
                                    right: sub.left - 1,
                                }]
                            }
                            (false, false) => {
                                /*
                                    sub    |------|
                                    p    |----------|
                                */
                                let asdf = 
                                vec![
                                    Range {
                                        left: p.left,
                                        right: sub.left - 1,
                                    },
                                    Range {
                                        left: sub.right + 1,
                                        right: p.right,
                                    },
                                ];
                                print!("");
                                asdf
                            }
                        }
                    }
                };
                output
            })
            .flatten()
            .collect()
    }
}

/*
A|-------|
B    |------|

A    |-------|
B|------|

A|-------|
B |----|

A  |----|
B|--------|

A           |-------|
B|------|

A|------|
B           |-------|
*/

impl From<&str> for Sensor {
    fn from(input: &str) -> Self {
        let a: Vec<&str> = input.split(":").collect();
        let point: Vec<Point> = a
            .into_iter()
            .map(|f| {
                let t: Vec<&str> = f.split("x=").collect();
                let t: Vec<i32> = t[1].split(", y=").map(|f| f.parse().unwrap()).collect();
                Point::new(t[0], t[1])
            })
            .collect();
        Self {
            point: point[0],
            size: point[0].dist(&point[1]),
            bacon: point[1],
        }
    }
}
struct Map {
    sensors: Vec<Sensor>,
}

impl From<&str> for Map {
    fn from(input: &str) -> Self {
        Self {
            sensors: input.split("\n").map(|f| f.into()).collect(),
        }
    }
}

impl Map {
    fn check_valid_points(&self, y: i32) -> i32 {
        let bounds = {
            let buffer = 10;
            let mut max_size = 0;
            let mut left = i32::MAX;
            let mut right = i32::MIN;
            for i in &self.sensors {
                if i.point.x < left {
                    left = i.point.x;
                }
                if i.point.x > right {
                    right = i.point.x;
                }
                if i.size > max_size {
                    max_size = i.size;
                }
                // let s =
            }
            (left - max_size - buffer, right + max_size + buffer)
        };
        let mut count = 0;

        let mut count = (bounds.0..bounds.1)
            .into_par_iter()
            .map(|x| {
                if self.check_open(&Point::new(x, y)) {
                    1
                } else {
                    0
                }
            })
            .sum();
        let mut all_beacons = HashMap::new();
        for s in &self.sensors {
            all_beacons.insert(s.bacon, true);
        }
        for point in all_beacons.keys() {
            if point.y == y {
                count -= 1;
            }
        }
        count
    }
    fn scan_by_coverage(&self, top_left: Point, bot_right: Point) -> Option<Point> {
        let a: Vec<Point> = (top_left.y..=bot_right.y)
            .into_par_iter()
            .filter_map(|y| {
                let mut c = Coverage {
                    ranges: vec![Range {
                        left: top_left.x,
                        right: bot_right.x,
                    }],
                };
                for i in &self.sensors {
                    match i.check_coverage(y) {
                        Some(r) => {
                            c.remove(r);
                        }
                        None => {
                            //out of range!
                        }
                    }
                }
                match c.ranges.len() {
                    0 => {
                        None
                        //this should happen most of the time!
                    }
                    1 => {
                        
                        assert!(c.ranges[0].left==c.ranges[0].right, "should be range size of 1!");
                        Point{x:c.ranges[0].left,y:y}.into()
                        //this shouldn't happen!
                        // panic!("covered the whole space!")
                        
                    }
                    _ =>{
                        panic!("more than one hole detected!")
                    }
                }

                // todo!();
            })
            .collect();
        match a.len() {
            1 =>{
                a[0].into()
            }
            _ =>{
                None
            }
        }
    }
    fn scan_range(&self, top_left: Point, bot_right: Point) -> Option<Point> {
        let a: Vec<Point> = (top_left.x..=bot_right.x)
            .into_par_iter()
            .map(|x| {
                let a: Vec<Point> = (top_left.y..=bot_right.y)
                    .into_iter()
                    .filter_map(|y| {
                        let p = Point::new(x, y);
                        if self.check_open(&p) {
                            Some(p)
                        } else {
                            None
                        }
                    })
                    .collect();
                // println!("done x={}",x);
                // std::io::stdout().flush();
                a
            })
            .flatten()
            .collect();

        if a.len() > 1 {
            println!("{:?}", a);
            panic!("this array is too big!");
        }
        match a.get(0) {
            Some(a) => Some(a.clone()),
            None => None,
        }

        // a
        // a
    }
    fn check_open(&self, p: &Point) -> bool {
        for i in &self.sensors {
            if i.in_range(p) {
                return false;
            }
        }
        true
    }
}

pub fn main() {
    let f = |a: &str, b: i32| {
        let map: Map = a.into();
        let covered_lines = map.check_valid_points(b);
        println!("Day15 : covered points {}", covered_lines);

        print!("")
    };
    // f(_FAKE_PUZZLE_INPUT,10);
    f(_REAL_PUZZLE_INPUT, 2000000);

    let f = |a: &str, b: i32| {
        let m: Map = a.into();
        let p = m.scan_by_coverage(Point::new(0, 0), Point::new(b, b));
        match p {
            Some(p) => {
                let t:i128 = (p.x as i128 * 4000000) + p.y as i128;  
                println!("Day15b: found hole at {}. Tuning frequency is: {}", p,t);
            }
            None => {
                println!("failed to find hole...");
            }
        }
    };

    // f(_FAKE_PUZZLE_INPUT,20);
    f(_REAL_PUZZLE_INPUT, 4000000);
}

const _FAKE_PUZZLE_INPUT: &str = "Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3";

const _REAL_PUZZLE_INPUT: &str =
    "Sensor at x=2150774, y=3136587: closest beacon is at x=2561642, y=2914773
Sensor at x=3983829, y=2469869: closest beacon is at x=3665790, y=2180751
Sensor at x=2237598, y=3361: closest beacon is at x=1780972, y=230594
Sensor at x=1872170, y=78941: closest beacon is at x=1780972, y=230594
Sensor at x=3444410, y=3965835: closest beacon is at x=3516124, y=3802509
Sensor at x=3231566, y=690357: closest beacon is at x=2765025, y=1851710
Sensor at x=3277640, y=2292194: closest beacon is at x=3665790, y=2180751
Sensor at x=135769, y=50772: closest beacon is at x=1780972, y=230594
Sensor at x=29576, y=1865177: closest beacon is at x=255250, y=2000000
Sensor at x=3567617, y=3020368: closest beacon is at x=3516124, y=3802509
Sensor at x=1774477, y=148095: closest beacon is at x=1780972, y=230594
Sensor at x=1807041, y=359900: closest beacon is at x=1780972, y=230594
Sensor at x=1699781, y=420687: closest beacon is at x=1780972, y=230594
Sensor at x=2867703, y=3669544: closest beacon is at x=3516124, y=3802509
Sensor at x=1448060, y=201395: closest beacon is at x=1780972, y=230594
Sensor at x=3692914, y=3987880: closest beacon is at x=3516124, y=3802509
Sensor at x=3536880, y=3916422: closest beacon is at x=3516124, y=3802509
Sensor at x=2348489, y=2489095: closest beacon is at x=2561642, y=2914773
Sensor at x=990761, y=2771300: closest beacon is at x=255250, y=2000000
Sensor at x=1608040, y=280476: closest beacon is at x=1780972, y=230594
Sensor at x=2206669, y=1386195: closest beacon is at x=2765025, y=1851710
Sensor at x=3932320, y=3765626: closest beacon is at x=3516124, y=3802509
Sensor at x=777553, y=1030378: closest beacon is at x=255250, y=2000000
Sensor at x=1844904, y=279512: closest beacon is at x=1780972, y=230594
Sensor at x=2003315, y=204713: closest beacon is at x=1780972, y=230594
Sensor at x=2858315, y=2327227: closest beacon is at x=2765025, y=1851710
Sensor at x=3924483, y=1797070: closest beacon is at x=3665790, y=2180751
Sensor at x=1572227, y=3984898: closest beacon is at x=1566446, y=4774401
Sensor at x=1511706, y=1797308: closest beacon is at x=2765025, y=1851710
Sensor at x=79663, y=2162372: closest beacon is at x=255250, y=2000000
Sensor at x=3791701, y=2077777: closest beacon is at x=3665790, y=2180751
Sensor at x=2172093, y=3779847: closest beacon is at x=2561642, y=2914773
Sensor at x=2950352, y=2883992: closest beacon is at x=2561642, y=2914773
Sensor at x=3629602, y=3854760: closest beacon is at x=3516124, y=3802509
Sensor at x=474030, y=3469506: closest beacon is at x=-452614, y=3558516";
