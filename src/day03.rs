// use std::slice::ArrayChunks;

use rayon::prelude::*;

const POINTS: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

pub fn main() {
    let binding = PUZZLE_INPUT.to_string();
    let lines = binding.split("\n");

    let total_score: i32 = lines
        .par_bridge()
        .map(|line| {
            let (a, b) = line.split_at(line.len() / 2);
            for c in a.as_bytes() {
                match b.find(char::from(*c)) {
                    Some(_) => match POINTS.find(char::from(*c)) {
                        Some(v) => return (v as i32) + 1,
                        None => panic!("invalid input, line includes not a-zA-Z!"),
                    },
                    None => {}
                }
            }
            println!("{},{}", a, b);
            panic!("invalid input, these bags are the same")
        })
        .sum();
    println!("Day 3 : total priorities:{}", total_score);

    let binding = PUZZLE_INPUT.to_string();
    let lines:Vec<&str> = binding.split("\n").map(|f|f).collect();
        // .into_iter()
        // .par_bridge()
        // .map(|j| j)
        // .collect();
    let total_score_b: i32 = lines.chunks(3)
        .into_iter()
        .map(|bag_data| {
            let bag_table:Vec<[bool;52]> = bag_data.clone().into_iter().map(|s|{
                let mut has = [false;52];
                for index in s.as_bytes() {
                    let i = POINTS.find(char::from(*index)).unwrap();
                    // println!("found {} aka {}", char::from(*index),i);
                    has[i] = true;
                }
                has
            }).collect();
            for item in 0..52 {
                let mut all_have = true;
                for bag_has in &bag_table {
                    all_have = all_have & bag_has[item];
                }
                if all_have {
                    // printthese bags:ln!("these bags: {},{},{} \nall have {}",bag_data[0],bag_data[1],bag_data[2],item);
                   return item as i32 + 1;
                }
            }
            0
            // let a = for s in strings {
            // }
            // for value in counts {
            //     if value == 3 {
            //         return value + 1;
            //     }
            // }
            // 0
        })
        .sum();

    println!("Day 3b: total priorities: {}", total_score_b);
}

const PUZZLE_INPUT: &str = "vGFhvGvvSdfwqhqvmCPnlFPnCNPcCFcWcr
ZbWZDMgsTHsrNNLJcJnsJl
HbBWQgZVZZBzbgZphwjqpmmVfdGmjG
vvCJLGnthChvtrvCCnRbTRqRPRBtbTRfPRRl
djZSgHNNwjqcdWlbcbfc
pFgMSfpMfzMDZFSgSjGJQQnCvMCVLnnJQLGC
gVhQWQpcWZVwwHVvFvnnnnDFdL
lzbPlztjltztzSjfGcPdTHLTHFCnnHCLndFGGd
jsNbzbczclttSlfbqlljRQMJRMMpJwRZhspZgJRM
hLJvfGcNDttSGvJtvSSJcqbqFBBWbjQqDrqbjDDjjb
lTswlzZdssgFpdPwZpMQnCjngCCjWBQBWWQqng
PRZMpzPZTdVZTfJvFvLFRctJcf
JHbQtHVHHLLbTJmmZddgdgwhllMNhhhTgg
spqpNGDjDPMhCFChMj
DBSDDGnpSDsDWqWczcvSqWvsBtJJLLZrRVZLJRbBZNVrBHrV
GwGhfhPhpHccvwSwrTsmsCjDmqTfbDqjss
tQntQcNRJMFnnVQFFctJqCsRmsTjjbDqDlTqTbWT
NZdVBZZNFzMFNNNvGprZcLGPGrrpcP
SvCPLrrlCSvZLrCPPBNPRvNLQBbpmbdggQTTfpfQgpgTqbbb
HVjHwMVwWtdMGwtwMwdhDFgbJgJqTmFMfFfmmfTTpq
whtHVcjDtHWtsWdwVGVHthDPzRrNSsCRLrLRlZNzZSNzNR
rjrlCCBtbtntwPPt
FvfJHqBJQWWgWgWLwnMwMLbzvhwTTp
WqHgWqBFgGfQgHfVdQFfVfrDllSsRSDmVmRCZZmSSSSZ
gjnppCgGHNPrqqmFnnbr
tGltVlJRtLRlrqcJcqZDqBJc
VvhLlvWltWdVltRTLTfgwSjGNHhggQNQNjps
HDWjCNfQjmwgWhcwPPVbZGcpMb
sFltFBRRSRJBSSsBlSSnRLPZbVGMVPZpMpPZpcMrFZMc
BLTBsstlqBRRBSvJJBsHdgdDNHCQHvbdCjQNNW
lRGzWLZNFwJVbVVGcJ
HqqpjPvHQnJgVgTnbdTV
rVvjBQHQrQhCrzlzWrRlDZ
jZjTZRSjZnhGZhzGnG
HbnPHrCbBDMnhcrVLWWLLWWg
MBwbCNDDptwNMttjdSnsqSRSFFdjtj
bSfvcsNsDdccHHQm
ljrlplvBhDHDHHHHJl
jgpzhrzRrvhFRFrzFnWfZtTwSWZPbbqNbsTqsW
vvCTvcDzHcgtvWjvcDcvgBCgwTdPFPpwpwmTSwwmdPwZpfZh
rNLMNVqLGNrVsRNJNsPnGnnfnpSFGdfddndd
VPQsMbVsPbzzvgCzgv
wPsrqprHQQZsChZn
cjgFLwWDlDltfLmTCnmWCnZZChCQ
FccccSLGFwjVlfVLLtgdSPSpMpBMdMBRRdHdBp
mQQcpmCCprrfLQqZVGqLGv
PtsJdsMtTTTvFqLTnnqbGZ
HjldthsHWztJzstZhBcHDDgpNpCpmrpgSD
lCmhDljDJgWggcnh
LdQrbdTDQGfGLPdqqFrHwRJcWRHgHBWHBJ
sLqsGtDqdGQfSTsqtfqqVMzjCzlvllZljNpCsMMl
CfLCZCCqqHlhSSrrtpRjpL
mWQbnQZVTWwNdwmDSbpbrFptjDrjRj
PJWVnTWPVnnclqqBsCCZHP
tqvtbNCgqJSgZgZvSncrrcGjBGhcnVcR
DswGQQQdRcjBnRDn
sMdlFMQQpfZbvlNtZGgN
RMlPllHtrlrlcZLsZfLcfwdDGD
QppnQhTBgwQDJsGzLQ
gTjnmjvphDSNMbtMbtMRHNVr
RHHcChrVVChCWQmRnMZmnmbTbGmFnqTT
gpzpfpszDwvDDNdwjdstnSMMHGMqvZnFTTZqbq
DsDdpfppwHsgJdjzfdDjdssLPlRccPQQrJcchPQWWQRhlQcr
rsrjQjnRnQZZqMmMMVqs
WTSTdvJLvTGJTGCMGvzBBpVVqqFVzBzVmf
tTSJhGLJbJhLJRRbHPQbnHHMrl
GGgMgBJHWHhLWMhWhgfrhgWLzmsmlzTtzHmsmlszRtszRVlT
bvScppfcQfcQSFCQpnPqwwTdRTvmzVssvswtddsv
ZDpqnPbQbPPnQbFbfSPSqbQJhGrjgMZBhWLrLrBBBBJWhg
hQCCGCNhDmGFJsTt
fcggBBpvBSrtsRTpRmpD
wflWlBlfnvfWWgMNPPLhPnzhPmLQ
pcGGTvVpcQLLzSPPPpVBVQwngNqgsJqgJgqSngsJMqJg
RZDFGhtCDGmWfWsNdwJhnMgwswqJ
ZCGjRGjZllFGHvvcTPjPTQpB
FDVsWrFZnnnfNRJdgBBBMLsJLH
wcThcTphvCThwTlblpzwGlpLRgHJWLgHLBHdHWJLBLWR
mwcbPmClwlzlwvvbTmWbQSjZmZSrDnSNVZfVFnZf
tMlttlFRSrcSFcwQSRwSzrMMPPGGPGLWgNfTNTcLPNPGBPPG
pbZVCDTqnCjVDHnHVnhBPBBbhBhbhgLgmWLh
qZCJZJqqjjCVvvRQltlzTrJtMMMw
QvvdBDdMbdFFJrMMjjmjCfCntC
lHTPsNLPcfVZLnfj
GgsWpHPpTPWpNsGvgdnDbQRQFRbdQg
jPNwllsVZjhslSjwGShZMdJDmmdmWLtMDDPHMPFd
QbvpDrbBrtMbbHJmcH
vznvzBRpBprQBqQZjNswwZDnSlGjZl
PdNTzLQPLrVMzGcMtt
FsSvDrvmrwDggHGwgV
vlpZpZmfnmFTlTWJWdbrdh
mZmnggMTSJrrmnrbmTbngJMtwPvwzzRvPGhQdGZPGPLvGvRd
FFVBNHVlFlDfCsWwLwPzzhGPCvzhQv
qBHLsDcfqFfDDfsFLNcNNBFsrgpMpnJSbnmTnrtmbMSqnpnr
qjBNwBPNPspqddssbsTsMDhTDrThQb
gvzZSZzFbgHnrHmn
fcvfZcRSZFGfZcvFbGttcPfpVjWVwqBqdwNdwNNpqV
RrTmtTrqznrnRCSqJrWlWDbhWVnfVDVWdclV
QBBgHQGvHHQswLHQQLGLHdLhfhlZZFlsscVDZfWfDhVlZZ
dLBMPpGPjLHPHPBHjjgQjHQztSJRztmrNCSCmSpmprrrrz
HHWJgjjsJrPBWBjgWgDvbbvtbNDNVtttMPPp
SnLTlhhNSntRVVLFVbbb
ChqdcNTNqqJCrQrQrrBC
BvfLLngFLDrrlDFDDnGmGlmzqzdGqMMWWwWW
RVsPbsbVZbjctccCcsCSPmdNqMqMWddwqVzhhNHwHd
tcsjPZRctZTSbbtSbtsSjZznnJfJBrfJgLvJJDDBvpTrgp
LJJsNdtJQtbWRJQttjGhjVnjcnzcsczGqj
MDPPlvCwrTlZfMMvTlPTdVcVhVVjchSBrjccSnnq
lgZCvTTZfMgHLptdRgmR
gCDrJRNgJDZRCwMgqGbtVVjTjlFbbTtR
mccnfcnSQScdvdcQQQpWdnWSjPqTbFFlbPqbPVGNjTPjtN
NnzmpWmBBzzpzDgwhDghrZrw
wcbVDBQwVBFQLFQDQcqQcLcJfpHJjmljGgMHfcfgGgjf
PtnWMtSnlgJmWWmm
nThPtRnzntstvrtRPqDFLMLdDwBBFLQDBT
zshqnVqTwqHqZQgZDSZjpFjFFF
PsBRvttdcgFFBSmc
GlPsbLtrvrrrtJlCTnVTlwwfnhwVqH
CvVVnFwWZnZwJZMNlCMNMpbMrrQG
cpghqzqqtzbGMjTNclGN
PqLBsgqBsSfBffShVmvRVwWsFpwZVpsn
LzsLSScvscqNdGdgddQjCDbzhpCDbRbhDpDDwDwt
ZlMBBBnlMFVFHVMJflJjJBfhRnCbCDpRttRPDCbWpCRbpW
mlrFmJrmscNGmsjm
FqQjLRjfvTFvlPHHNPMBDDNDPR
chWptpcWTzBPMsMMMBHW
zcJghwJZpZcgnctccdzzpGQrbQbblFFlTCCFTbdCFCFq
QcwNpCcQzpwtCGPPPnrGrfHfvN
FgjhhhjMVFVjqRRqDBVBqvMZvdrHnPZWZvsnZHdWnv
ShBFhDVghhTBgBBFRRgRCmCPCcpPbwCmLTcCmbpb
ZzlzsBzZnWnsBhFRvfvvLfWqfvMv
GgQGjjddHHPwpHpTGjPdHMvtvwJqLtJRFlRFRqMwLc
dgbbdjpGGgTHGGgQgdpmpgblrBrhrhCBSSznChsVhBsmNB
PhSwPdnpsmSWWcjjDFNqnc
GJGCTLbTZTrlfflVLFchHDHHDFcVVN
RhlhrllQZhCsPvRBvMtSvw
GVgnrgTWGVGjLVjWSpvvNmPTmpQmzvhf
tBbBDsFtszzSSbPZbh
BqlqdcqSJtFMdMjWrnGCWRrWGG
sJVJsQhMhPPSQMwdHRmmsmmwRmsr
zLFjLDTBFNWWwrqrffldlRdFRq
BLjzjTpzLpzWGTbQpMJvVMQwQhhMQM
RqSztDRhJDLmRMLlfvsP
dMdMMHZCsnbdvmbP
VHcZVVcZTwTQpgHQcgFMFBpDDzBqqqDhqJSjSjJjSD
rdMnMGjdHhfnjqWWDJPpGWPtvW
PTBSQSSzZSBSCzQFFSlZTFSvcZpWWcJvtJppvZpLLcDVcv
sgQCsBmmlFCPFFzTgTBgdNnrNndnMrrrfbrNjf
bpZdggTjHbgLglpHjldvHpjdhTVzmhzzzFPzmhFsFQSFnhhN
rPCDBcCCMPGcWDNNWQzQQSNQnNzQ
fCCJtDtGGGfGBtGqBrrcfRBcvPwpLgdZZvHdljvqpvdwbpvl
qpmsNldnlHlCqQlHsHNHwJpJMtwvvvjMvfWjpDtt
TccVBScrzBzzTGPbVTPQhWjfMjwwRtJtjMtWDWWfDS
FQFbzBGczGBFLnFmmqsCLg
qpblblvpvJzStJDrhrnGrdhDfFqf
ggNQNwBgmTcgCBTBTQQjNfDCnZRRRrRGCnrFfdnhrC
mjVFHQTHNjTwcmpzJzHltltbSssS
WChWmdcmzndhFcZrrbvrVMVssj
NQLDlDplpSJGpLfRRMZVBBGjVsGbbjbBZsGv
QNDfNqlpLgSfNfNgNfpgpqwndwWwnCCnnTFMdHndzn
ZGRPTngTZMSGMGnhSgRjQHsPbqjmsWHQCQWbNN
BFLLfpzVDBfDdlfQcsbVcNmQsqqbcC
zFvdplLDLtzFmrrwMMtTrShZ
nTdmnVCGqTsSBTqv
HlMPwMlHfPSfBBmFBfSL
trHHwRHRwMHPMJQJHnDhbdRhdpCZmChNnd
lwHWjzplvHqWHGsMLsLwLfgdfLdg
tPJNPQmQmSGcTtFmctGmSCBgsBBRbLBRVdLVLCBBLgVf
FPtTPQNPrPPQctTcNPSQJJPDjlzplnGDDjWWplWHhGvpnr
jwvvDbvsRsrrjrfvfrrZsPpCpmPJJPqlqWmzRJRTqq
HdLttdSQHdLHMMtNdLMSTtHpCmPplWhCzmzmPWlJhlNplP
BTSLtLLQtnVZDnffbwfw
snvQdrtrQprWpgmGLp
FhzwlwHccBcljFBSDmHmLpgRmPDCffWL
zFhllqjczzwJqqSqlZMsvJgVNMTbssVNnQbb
dLZHrWjWPFZWZnPjZttjddFnMDVMGJMQqvMVGVRVpjVpGVvv
zhzTwTlfTwCsShSgNhfzsQQqvMQStQMDGJJGvGQVvq
TTwCBfsfBwhzwTBCzlmHZdLmBBbtLnLbFnnF
BhBRLFmlBlmhgShHmhSlZlFgvbQNwvcsvMCcsQCwNQvNMsBw
ttWddDjrfjDcssscDbvH
jfjfPWdzdfjdnWpjtrzqnhmJGJFhSJRHSmSmlqlHmq
QvJTgvsvghHRHHNbZvNZTRSzBBCLrDqzrfDDtJSqSLBC
cPPwdcFFPDwfFrBrFfFfrC
nppwdplpRvllsgDH
BGLLWLLwHVZwHnNhwsMsrqMqhh
STlTpDpmjzmjjjgccqdsbNbBhlNnNMhsqs
pjmgjfSDSzmSgFzRZQfRRZLBVLVtZZ
WHjddztMtVLNNFFTmbFPFPRw
bJnvpQfqccQJZfpcbvCphcFGDPFGRwDGDDGwGsmPFnPF
rvffQJZJqrpZCJZJQrQpvBvStlSWSzVSWBbzBLzjtjWWWj
SJFMSMGSDLTsFgHvHL
mNzRrRRzjzqqgPHvLTHjlvWg
ZLbRpRnRnCrbmnmrRRNnwbGfMwDwfDDdSVMdVJdd
nsqTbhcDssPsPWsnchPJMSTSMmJMwTSTCJJfJw
DHvFvvdHpvpGFHDMVVJVplCCVpggCl
RdQjtvHtDQNGsZqzcqPqbNcq
GFzRjczzQJnLjJvvTj
mSfHrNHDzHDrDSSSBvTZLTNqWJWBWLlv
PfgCmfPzDVrtHsddVMsRFcVFQM
sfBgfBfBsHBHFGhsqfjgQZtQQMdZgbZQptbM
rNLRSzRTrrvvLSTWGpjpNZdQPtGtMdNM
wvSWLwzTGTCcwwwJwvwcrcRcVfFBqhhVhDqhBllBfFqBCDCs
LbTpDTcMTSzzMLhScnDnSppNQwVNZFBVnFsrwQQZrQrQrN
tJtJCRlGWljGWCtjJZVPsJBsVwQmrQNN
fvHRftqjGfWGwtfGqvLTzDTzzzchSbMDTd
JJhWZlhqLDHtBDrqrB
bwwmfrSmbmFjVSFQwSdpDvGdpPnRvDtHpGtGPG
VgFfcSQfFgmLLrNThllTZc
QmfvrpnvrrJGnBSCFTBMSWFS
NVMggbVPzCTgDFDD
NRqHRZjVRZdRVdZwNPrMrchGhGpcGfvhQlHJ
nlBdCldndlZTttSSBBccPfGWLLHcTTcWPbbW
jpsFzFmzDzNzDGChGcGGmPQHQf
CNzqvvVJNFqvgRtlqRtdnwSZ
MJtDbNHDDpmVPJVzzjLm
RslhvlfRTWvWWRwfllSZngmggznjSPznLjmSnz
TlhffRwWQhChDqbBQLFHqNrb
HWnmSbzflWltlzLfWWDzjMBvCjjCTCgcMvzBBB
qRRRZJwhZFGdRNfghVjMVVgfcghC
QFFqwNNNwdNZZpqqZfnDSHmmlDLtWtHWlQmD
JrFdNTTLRBTJrFVrBNdVLFBdlHbzQQsQzbPJtpbtltWsHbQw
gZffDfMlCfjCSqMcpHPWHszstzHjQwpb
GMnvfGlfvSqvcMMDgDDcfnqSVFRdmTmVdNBTdmRmBFVnLLBT
ZvRHtDcZntLZssMssQBrMdnC
jglqlVdlbqgVWjJMbrrBCpmQBBfrpm
GqVVPPjPNjFVllNjJjFDvzTLZRvcLRLvTGdGDv
fDVzvVfzzZPMsMbb
LHtBwLBdhFgdHLLthRwFGGMsmrHMmmbZSmqbMGGG
bblRwbTRlllfVQnCQn
fVZzjRzpzpVCRPZhVWQvvLsWWWFQlmjWmG
JDHgJdtwbZqJqsWBBDLlQlvLms
cHHtdbqwrqbbtSbTgSTcVCMRZnRRVNhVPNMPpfMc
fCMPBBdpMpsqMssQccnV
TlwGWDjDZHLjZHHlLGmnlnNcRllsJcqtsJRV
ZHhZHThLLrGwjDDjLwGWWWSjPpbpzrvBFBFdBBbqvCbpFfpg
bvDfDPtCVfFFVdWWpmLRmzWzzdBW
jgZTghhjrGrsswrsghHrlgTGzSRmMLwpJMSMzWLLWRzpRRSc
lpjTpGHlpsNGTHllHrCFnDNPtVnvfQtQtQNP
BwlQcwZBwwwQNqJTrrsRGCDTNt
bMpVPSfRvCbCtTqsCs
dPSRfRpPPjjmLMgZBZBLZZwFQnZn
TsVfggTqVnsLVTdTpmDdRhwPRtPRDRwD
ljHBSWZvvFWvBFPppnQPwnpmzR
MSHSjbBclBSjccLJgqgnLVqTbsTT
zncfVgRzVJgnTfVqNHvZJZNJNMpHbdvH
PCBpsLjPPmMGdHNdHBHZ
jSLlrhLPDWLrPrDCLPCfnpzcVRFcRTnlVncntT
ccvSgjHtRjcjSvjvSrBjzSHHwnJPbgwPPZVVVZnPpZlpwnlT
ffqNqGGsGWqLTNqZwdndPnnJJpZVJW
sNmqhsCMQsMTmjrcmrHrRj
gWWWzNVJDwDzVWVDGbGNnhTnHLsmhmhfsQTNSmHd
vtMPZvrZvqtqBHljrqSnnmTfLfdnQsjhjmhL
MMPZPBqZCrBtvZcrBlDbGbbbzHJCbbRwgwDz
hGSRhsMswhcNNGwhwncMnCqCJNrHJPJJrJtCJdqHJm
BgVTzWBdTfCmfCJH
dlbVFvvWVZhSRQDMnlhc
WfpzBZmgJlQVGvWF
wHSbrHwmccnrmrHsClGqFGbQjQjjQJQQGv
SHPwwsRcrrNtrNSsphmZLzpfzhghZPdD
DFDPRpmgbPQtmgBBQDDNJTMMBZsqsZGqGZTGCGSqWG
fVvVVLcJVzlvzhqfSTCsZsMGMHqq
zJdrrdnzcLlwczwbQmQngRDQPbtDpQ
HDZZrpFqwRrQfBqhjjlVlQ
czTgvvWPNgPGcTlsQflCVshClC
PgNvtSJNvGVMMzNzgGvPGGLHFDdFdmZSZRdDdRrmpFwL
SVHNVFVPBHJqHhgFCgzLmCwppm
DvDdsGZljDlfdZnjnnZGMzLpRgLfMCLmzfPLhmgp
jZDZlrvrZTrTrTQrDsjslHNJqtWbWHbqbPBWNVQWBJ
NmGGBdWWJDJTTZHm
hFVhcqFjncpcppSjqfppqDvzDDDbbDZvDZZbHfJgvJ
jrPqnnHnqSPwPGWPdWst
BfhbwMwbbPbHPPPlrdJjrlMJLrJVTd
pnQnGnWDjnJdlJCh
qshsWvpttzNNQDtzRRPvwfcPHBBBHwbw
SHzGRQjhwwhGzjjwRjfBqpqbNCqNnnqqQqPlQC
TZtgLmZgVmgdFgmZtdrbNqnqlNlpblnlrnBd
DvgmvvZgmWJJjwHHhJSzps
JjlrlJjPJgDjJjJnDRDjNwGGqMvSddvPvwQddqSVvq
SLpphFLhFZhWLzvswwWqsqVVQWdv
FSHtTLZpfzRDDrJgRNjT
wjCMvrMlqqWHvWqddrHqgnBNhcffthhVLtpgLBnw
GbQFZzZZphnpgNZV
FhFzRTDRPzsRQGQGTFlllrJHjdsJlHMqjjHr
LqDcTbmJcqSJSTmnrTcmJrfffplfjZsGZfGGZfQLdplj
hWddgBvzWFZfPsQlGh
RBWBRCdHtgHttVVzHBVNNNDwSTDcSSSbScDDwbwbnmRS
FFPzwlZVVrzFFlFLVlllZdHCHPQMnJQQbhhChdhCbb
BRRqGBgRfqvgvBDDDTRgghNCMMTQNNbVJMNJJdbbdT
DfpgjGfsRWrFVzwLcs
PMTSdSmFjhFpNTqvppvRBrRBrDqB
HnZZznJbzGZGlZtZWHlJGcGcwMvQBsrwRDQvcDgrgDgrqRvq
HGlGfnJZfMMCfNhm
nRssqlqVRppVwdMMQwFgtRFz
smTvLLTvvNLtwMMQNg
CmPGBvZGWvBSGGDmTZjZlhpJcpHDJsbDnlrrprpl
djcQGNQqdGdGqMCgndwgCLDMgW
nvBvHpBppnvPPnJTBWLJVMwVfWJfCbfWgW
hsHHpBsvRTHpsPszTBTTsRTslGqGqlcqlScnqmhZmmZSZSjl
DddBHCmfWCBTDBHTHfMpzhzpJJMJsFrGrz
tPVPmbnttjPnZvSvSbnmZPZPNpNGMpJNzzNrGJpvhsshMpFs
mwnZcbmmStbVtVjbZVlcLTBlcLCRHRDWCWWW";
